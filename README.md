# Projet T-NSA-700

## Pré-requis du projet

- Node / [Téléchargement](https://nodejs.org/en/download/current)
- Docker / [Téléchargement](https://www.docker.com/products/docker-desktop/)
- Un compte Clever Cloud / [Inscription](https://www.clever-cloud.com/fr/)
- Un compte Amazon AWS / [Inscription](https://aws.amazon.com/fr/)

## Parties communes

Deux méthodes de déployement sont possibles, une grâce à Clever Cloud et Bucket S3 d'Amazon et une autre grâce aux instances d'Amazon Lightsail.

La création du GitLab et de son runner ainsi que la création de la base de données sont communes aux deux méthodes.

Un README.md est présent dans les dossiers Clever Cloud et Bucket S3 si vous choisisez ces méthodes.

---
## Configuration des projets

Peu importe la méthode sélectionnée, il faut modifier les fichiers `environement.ts` et `environement.prod.ts` situés dans `frontend-bucket-s3/src/environments/` ou `frontend-lightsail/frontend/src/environments/` et modifier la variable `api_endpoint` par l'URL de votre back-end.

Exemple : `https://api-nsa.killianbanchereau.fr/api`

> **Note:** Pensez bien à ajouter le `/api` à la fin.

---
## Création des machines Docker

Il faut d'abord créer les deux machines Docker.

Commencez par exporter la variable d'environnement `$GITLAB_HOME` avec la commande suivante :

```bash
export GITLAB_HOME=$HOME/gitlab
```
> **Note:** Adaptez le chemin à votre système d'exploitation et à l'endroit où vous souhaitez sauvegarder les fichiers.

Maintenant, créez le conteneur du GitLab depuis votre terminal :

```bash
docker run --detach \     
  --hostname local-gitlab \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ee:latest
```

Puis celui du GitLab Runner avec :
```bash
docker run -d --name gitlab-runner --restart always \
  -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

> **Première note:** Si vous êtes sur Windows, il faut remplacer le chemin des volumes par le chemin de votre dossier partagé. Par exemple, remplacer `/Users/Shared/gitlab-runner/config` par `C:\Users\Public\gitlab-runner\config`.

> **Seconde note:** La création du GitLab est plutôt longue, elle prend environ 5 minutes. Passé ce délai, il est possible qu'une erreur ait eu lieu.

---
### Configuration des conteneurs

Une fois les deux containers Docker créés, il faut configurer les machines.

Depuis votre terminal, exécutez la commande pour obtenir l'IP du conteneur GitLab :

```bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' gitlab
```

Toujours depuis votre terminal, exécutez la commande :

```bash
sudo nano /etc/hosts
```

À la fin du fichier, ajoutez la ligne :

```
IP  Hostname
```

Dans mon cas, la ligne est :

```
172.17.0.2	local-gitlab
```

>**Note:** Le hostname est celui que vous avez donné grâce à la commande de création du conteneur GitLab.

Sauvegardez le fichier puis quittez.

Répétez la même opération depuis le container gitlab-runner.

Il faut maintenant lier le runner à votre GitLab. Pour ce faire, rendez-vous sur votre GitLab à l'adresse http://localhost/ puis connectez-vous.

Pour se connecter, il faut récupérer le mot de passe. Pour ce faire, exécutez la commande :

```bash
sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
```

Votre identifiant est `root` et le mot de passe est celui récupéré précedemment.

Une fois fait, cliquez sur les trois traits en haut à gauche de l'interface, une petite fenêtre va s'ouvrir, cliquez sur Admin.

Depuis cette page, survolez l'onglet CI/CD situé à gauche puis cliquez sur Runners.

Cliquez sur New instance runner. Choisissez la plateforme de votre choix, ici macOS, puis cochez la case Run untagged jobs. Une fois créé, gardez le runner-token de côté.

Le runner est prêt à être lié à GitLab, depuis votre terminal, exécutez la commande :

```bash
docker run --rm -it -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

Les informations à renseigner sont :
```
GitLab instance URL : http://IP (http://172.17.0.2)
Token : runner-token récupéré précedemment
Description : optionnel
Tags : optionnel
Executor : Docker
Default Docker image : node:latest
```

Maintenant, votre GitLab est fonctionnel et prêt à exécutez vos CI.

---
### Configuration de la base de données Amazon

Rendez-vous sur la page d'accueil d'Amazon Lightsail puis cliquez sur l'onget « Databases » puis sur « Create database ».

Choisissez MySQL 8.0.33, le plan « Standard » puis sélectionnez le premier palier.

Une fois que le base de données est créée et opérationnelle, cliquez sur les trois petits points puis « Manage ».

Depuis l'onglet « Connect », récupérez les informations suivantes :
- User name
- Password
- Endpoint
- Port

Dans l'onglet « Networking », cochez le mode public.

Grâce à ses informations, connectez-vous à votre base de données avec un logiciel comme MySQL Workbench puis créez un nouveau schéma avec la commande :

```sql
CREATE SCHEMA `nom-du-schema` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

Enregistrez bien le nom du schéma, il sera nécessaire pour la CI.

Votre base de données est maintenant prête à être utilisée.

---
### Création des instances Lightsail

Rendez-vous sur la page d'accueil d'Amazon Lightsail puis cliquez sur l'onglet « Instances » puis sur « Create instance ».

Sélectionnez « Linux/Unix », « OS Only » puis « Ubuntu 22.04 LTS ».

Une vois votre machine créée, cliquez sur les trois petits points puis sur « Manage ».

Cliquez sur « Download default key », ouvrez le fichier téléchargé avec un éditeur de texte puis copiez son contenu.

Dans l'onglet « Networking », cliquez sur « Create static IP » puis sur « Create ».

Toujours dans l'onglet « Networking », cliquez sur « Attach » puis sur « Attach static IP ».

Ajoutez également une règle dans le firewall pour autoriser le port 443 (HTTPS).

Répétez les mêmes opérations pour la seconde instance.

---
### Configuration des Ansible

Dans les fichiers `hosts` situés à la racine des documents Lightsail, remplacez les adresses IP par celles de vos instances.

Si vous avez votre propre nom de domaine, modifiez la tâche « Get Let's Encrypt SSL certificate » du fichier `roles/apache/tasks/main.yml` en remplaçant `xxx-nsa.killianbanchereau.fr` par votre nom de domaine.

Modifez également le fichier de conf situé dans `roles/apache/templates/xxx.conf` en remplaçant `xxx-nsa.killianbanchereau.fr` par votre nom de domaine.

Si vous n'avez pas de nom de domaine, modifiez les fichiers `playbook.yml` et commentez les rôles `ssl`.

Modifez également les fichiers de conf avec ceux-ci :

`backend-lightsail/roles/apache/templates/laravel.conf` :
```conf
<VirtualHost *:80>
    ServerName 127.0.0.1
    DocumentRoot /var/www/html/laravel/public/

    <Directory /var/www/html/laravel/public/>
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/laravel-error.log
    CustomLog ${APACHE_LOG_DIR}/laravel-access.log combined
</VirtualHost>
```

`frontend-lightsail/roles/apache/templates/angular.conf` :
```conf
<VirtualHost *:80>
    ServerName 127.0.0.1
    DocumentRoot /var/www/html/angular/

     <Directory /var/www/html/angular/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
        RewriteEngine On
        RewriteBase /
        RewriteRule ^index\.html$ - [L]
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule . /index.html [L]
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

---
### Configuration de la CI

Rendez-vous sur votre GitLab local puis sur le repo sur lequel vous souhaitez déployer le back-end. Cliquez sur « Settings » puis « CI/CD » et enfin « Variables ».

Ajoutez les variables suivantes :

- SSH_PRIVATE_KEY : Clé SSH de votre instance Lightsail
- DB_HOST : Point de terminaison de la base de données
- DB_PORT : Port de la base de données
- DB_DATABASE : Nom du schéma de la base de données
- DB_USERNAME : Nom d'utilisateur de la base de données
- DB_PASSWORD : Mot de passe de la base de données

Pour le frontend, il faut ajouter :

- SSH_PRIVATE_KEY : Clé SSH de votre instance Lightsail

---
### Déploiement

Il ne vous reste plus qu'à lancer le déploiement de vos instances.

Les déploiements prennent environ 10 minutes pour les instances Lightsail.

> **Note:** Les sites sont quand même accessibles lors d'un nouveau déploiement. Vous n'aurez donc pas de temps d'arrêt.