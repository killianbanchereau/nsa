#!/bin/bash
mysqldump --defaults-file=/home/ubuntu/.my.cnf -h {{ DB_HOST }} --port {{ DB_PORT }} {{ DB_DATABASE }} chat_messages > /home/ubuntu/backup/chat_messages_$(date +%H-%M-%S_%d-%m-%Y).sql