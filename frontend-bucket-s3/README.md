
# Configuration du bucket S3 d'Amazon

Une fois votre compte créé, rendez-vous sur la page d'accueil d'Amazon puis cliquez sur « Services » puis « S3 ».

Créez un nouveau compartiment, donnez-lui le nom que vous souhaitez puis dans l'encadré « Propriété d'objets », sélectionnez « Listes ACL activées », dans l'encadré « Paramètres de blocage de l'accès public pour ce compartiment », décochez la case « Bloquer tous les accès publics », une fois fait, validez la création.

Une fois le compartiment créé, cliquez dessus puis sur l'onglet « Propriétés ». Descendez tout en bas sur l'encadré « Hébergement de site Web statique » et modifiez-le.

Sélectionner « Activer » pour l'hébergement de site Web statique, vérifiez que « Héberger un site Web statique » est sélectionné puis dans « Document d'index », renseignez « index.html ».

Sauvegardez les modifications puis rendez vous sur l'onglet « Autorisations ».

Dans l'encadré « Stratégie de compartiment », cliquez sur « Modifier » puis collez le code suivant :

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::nom-du-compartiment/*"
        }
    ]
}
```

> **Note:** N'oubliez pas de remplacer « nom-du-compartiment » par le nom de votre compartiment.

Sauvegardez les modifications puis rendez-vous sur l'encadré « Liste de contrôle d'accès (ACL) ». Cochez la case « Lecture » pour la partie « Groupe de livraison des journaux S3 ».

Votre bucket S3 est maintenant prêt à être utilisé. Il faut maintenant créer un utilisateur IAM.

---
## Configuration de l'utilisateur IAM.

Rendez-vous sur la page d'accueil d'Amazon puis cliquez sur « Services » puis « IAM ».

Une fois sur la page, grâce à la navigation en haut à gauche, rendez-vous sur la page « Politiques » puis cliquez sur « Créer une politique ». Choisissez l'éditeur JSON puis collez le code suivant :

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::nom-du-compartiment",
                "arn:aws:s3:::nom-du-compartiment/*"
            ]
        }
    ]
}
```

> **Note:** N'oubliez pas de remplacer « nom-du-compartiment » par le nom de votre compartiment.

Cliquez sur suivant puis donnez un nom à votre politique.

Maintenant, rendez-vous sur la page « Utilisateurs » puis cliquez sur « Ajouter un utilisateur ».

Donnez-lui un nom d'utilisateur. Dans l'étape 2, cochez l'encadré « Attacher directement des stratégies existantes » puis sélectionnez la politique que vous venez de créer.

Cliquez sur suivant puis sur « Créer un utilisateur ».

Une fois créé, rendez-vous sur la page de l'utilisateur puis cliquez sur l'onglet « Informations d'identification de sécurité ».

Sur l'encadré « Clés d'accès », cliquez sur « Créer une clé d'accès ». Sélectionnez « Application exécutée en dehors d'AWS » puis « Suivant ».

Créez la clé d'accès puis enregistrez bien la clé d'accès et la clé d'accès secrète, elles seront nécessaires pour la CI.

---
## Configuration de la CI

Rendez-vous sur votre GitLab local puis sur le repo sur lequel vous souhaitez déployer le back-end. Cliquez sur « Settings » puis « CI/CD » et enfin « Variables ».

Ajoutez les variables suivantes :

- AWS_ACCESS_KEY_ID : Token utilisateur IAM
- AWS_SECRET_ACCESS_KEY : Secret utilisateur IAM

Vous pouvez maintenant déployer le front-end comme vous le souhaitez, la CI s'occupe du reste.