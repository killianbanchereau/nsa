# Configuration du déploiement Clever Cloud

Il faut maintenant préparer la machine Clever Cloud qui recevra le back-end en PHP. Une fois votre compte créé, rendez-vous sur la page d'accueil de Clever Cloud puis cliquez sur « Espace perso », « Créer » puis « une application ».

Choisissez PHP puis git. Configurez votre machine comme vous le souhaitez puis appuyez sur le bouton « Créer ».

Une fois la machine en place, il faut récupérer vos tokens permettant à la connexion de la CI et l'ID de l'application. 

Pour l'ID de l'application, copiez-le simplement depuis la page d'accueil de votre application. Il est situé en haut à droite et commence par « app_ ».

Pour la connexion, depuis votre terminal, installez les clever-tools avec la commande :

```bash
npm install -g clever-tools
```

Une fois fait, tapez la commande :

```bash
clever login
```

Connectez-vous puis enregistrer bien le token et le secret affichés, ils seront utiles pour la CI.

Rendez-vous sur votre GitLab local puis sur le repo sur lequel vous souhaitez déployer le back-end. Cliquez sur « Settings » puis « CI/CD » et enfin « Variables ».

Ajoutez les variables suivantes :

- CLEVER_TOKEN : Token Clever Cloud
- CLEVER_SECRET : Secret Clever Cloud
- CLEVER_APPLICATION_ID : ID de l'application Clever Cloud
- APP_URL : URL de l'application Clever Cloud (https://CLEVER_APPLICATION_ID.cleverapps.io)
- DB_HOST : Point de terminaison de la base de données
- DB_PORT : Port de la base de données
- DB_DATABASE : Nom du schéma de la base de données
- DB_USERNAME : Nom d'utilisateur de la base de données
- DB_PASSWORD : Mot de passe de la base de données

Vous pouvez maintenant déployer le back-end comme vous le souhaitez, la CI s'occupe du reste.